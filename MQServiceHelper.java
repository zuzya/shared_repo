package ru.hostco.service.helper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import javax.xml.bind.JAXBException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

import ru.hostco.service.exception.InitializeMQConnectionException;
import ru.hostco.service.exception.MQHelperLayerException;
import ru.hostco.utils.xml.UniversalUnmarshaller;

public class MQServiceHelper {

	private static final String KEY_OUT_QUEUE = "out_queue";
	private static final String KEY_MSG_REQUEST = "msg_request";
	private static final String KEY_MSG_ID = "msg_id";
	private static final String KEY_TYPE_HEADER = "type_header";
	private static final String KEY_TYPE = "type";

	/**
	 * таймаут ожидания ответа из очереди
	 */
	private int timeout_seconds;

	/**
	 * Инициализированная ConnectionFactory для MQ
	 */
	private ConnectionFactory factory;
	
	/**
	 * Инициализированное соединение
	 */
	private Connection connection;

	private static final Logger logger = LoggerFactory.getLogger(MQServiceHelper.class.getName());

	/**
	 * Паблишинг сообщения в очередь и получение из ответной очереди
	 * 
	 * @param request - JAXB-запрос
	 * @param requestClazz - класс запроса для маршаллинга
	 * @param responseClazz - класс ответа для анмаршаллинга
	 * @param outQueue - имя очереди, в которую паблишится сообщение
	 * @param typeHeader - заголовок сообщения: тип
	 * @return
	 * @throws MQHelperLayerException
	 */
	public <R, A> A publishMessageAndGetResponse(R request, Class<R> requestClazz, Class<A> responseClazz,
			String outQueue, String typeHeader) throws MQHelperLayerException {

		logger.info("exchange with MQ started");

		if (request == null) {
			throw new MQHelperLayerException("nullable request is tooooo bad!");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("recieved params:");
			logger.debug("outQueue: {}", outQueue);
			logger.debug("typeHeader: {}", typeHeader);
		}

		Channel channel = null;
		try {
			
			try {
				channel = createChannel(getConnection());
			} catch (IOException | TimeoutException | InitializeMQConnectionException e2) {
				e2.printStackTrace();
				logger.error("error creating channel to MQ!", e2);
				throw new MQHelperLayerException("error creating channel to MQ!");
			}

			// сериализуем запрос в строку
			String msgRequest = serializeRequest(request, requestClazz);

			if (logger.isTraceEnabled()) {
				logger.trace("request to queue: {}", msgRequest);
			}

			// строим мапу с параметрами запроса
			String messageId = UUID.randomUUID().toString();
			Map<String, String> props = new HashMap<>();
			props.put(KEY_OUT_QUEUE, outQueue);
			props.put(KEY_MSG_REQUEST, msgRequest);
			props.put(KEY_MSG_ID, messageId);
			props.put(KEY_TYPE_HEADER, typeHeader);

			if (logger.isDebugEnabled()) {
				props.forEach((k, v) -> {
					logger.debug("filled properties header: {} = {}", k, v);
				});
			}

			// публикация сообщения
			String callbackQueueName = sendMessage(channel, props);
			// получение ответа
			String msgResponse = getAnswer(channel, callbackQueueName, messageId);
			logger.debug("recieved answer from queue: {}", msgResponse);
			
			//десериализация ответа
			A serviceResponse = deserializeAnswer(msgResponse, responseClazz);
			
			logger.info("responding answer to service");			
			return serviceResponse;

		} finally {
			
			closeChannel(channel);
		}

	}

	/**
	 * Отправка сообщения в очередь
	 * 
	 * @param channel - канал соединения с MQ
	 * @param props - мапа с параметрами
	 * @return -
	 */
	private String sendMessage(Channel channel, Map<String, String> props) {

		logger.debug("sending mq message...");

		String callbackQueueName = null;
		try {
			//получаем имя callback-очереди
			callbackQueueName = channel.queueDeclare().getQueue();
			Map<String, Object> headers = new HashMap<>();
			headers.put(KEY_TYPE, props.get(KEY_TYPE_HEADER));
			
			AMQP.BasicProperties msgProps = new AMQP.BasicProperties.Builder()
					.deliveryMode(2)
					.correlationId(props.get(KEY_MSG_ID))
					.messageId(props.get(KEY_MSG_ID))
					.headers(headers)
					.replyTo(callbackQueueName)
					.build();

			if (logger.isDebugEnabled()) {
				logger.debug("AMQP corellationId: {}", msgProps.getCorrelationId());
				logger.debug("AMQP messageId: {}", msgProps.getMessageId());
				logger.debug("AMQP replyTo: {}", msgProps.getReplyTo());
				logger.debug("declared callback queue: {}", callbackQueueName);
			}

			//шлем сообщеньку напрямую в очередь, без указания exchange'a
			channel.basicPublish("", props.get(KEY_OUT_QUEUE), msgProps, props.get(KEY_MSG_REQUEST).getBytes());

			logger.info("message sent to queue: {}", props.get(KEY_OUT_QUEUE));
			if (logger.isTraceEnabled()) {
				logger.trace(props.get(KEY_MSG_REQUEST));
			}
		} catch (IOException e) {
			logger.error("error on sending mq message\n", props.get(KEY_MSG_REQUEST), e);
		}
		return callbackQueueName;
	}

	/**
	 * Метод ждет ответа из сгенерированной очереди
	 * 
	 * @param channel - канал
	 * @param callbackQueueName - очередь, сгенерированная при запросе
	 * @param messageId - 
	 * @return
	 * @throws MQHelperLayerException
	 */
	private String getAnswer(Channel channel, String callbackQueueName, String messageId)
			throws MQHelperLayerException {

		logger.debug("getting mq answer...");

		QueueingConsumer consumer = new QueueingConsumer(channel);
		String consumerTag = null;
		try {
			logger.debug("consuming callback queue: {}", callbackQueueName);
			consumerTag = channel.basicConsume(callbackQueueName, true, consumer);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("error while trying consume message", e);
		}

		String answer = null;
		try {
			while (true) {

				logger.debug("waiting for next delivery...");
				QueueingConsumer.Delivery delivery = consumer.nextDelivery(timeout_seconds);
				if (delivery == null)
					throw new MQHelperLayerException("message is null or message do not retrieved in timeout "
							+ timeout_seconds + " milliseconds");

				logger.debug("delivery recieved!");
				
				String correlationId = (String) delivery.getProperties().getCorrelationId();
				logger.debug("corellation id = {}", correlationId);
				
				//если сообщение - наше, забираем ответ
				if (correlationId != null){					
					
					if (correlationId.equals(messageId)) {
						logger.debug("delivery body \n {}", delivery.getBody());
						answer = delivery.getBody() != null ? new String(delivery.getBody()) : null;
						break;
					}		
				}						
			}
			channel.basicCancel(consumerTag);
		} catch (ShutdownSignalException | ConsumerCancelledException | InterruptedException | IOException e) {
			e.printStackTrace();
			String msg = "error while trying process delivery: ";
			logger.error(msg, e);
			throw new MQHelperLayerException(msg, e.getCause());
		}

		logger.debug("responding answer from queue\n {}", answer);
		return answer;
	}

	/**
	 * Сериализуем запрос 
	 * По дефолту маршалим xml, можно переопределить, например для JSON
	 * 
	 * @param jaxb
	 * @param clazz
	 * @return
	 */
	protected <R> String serializeRequest(R jaxb, Class<R> clazz) {

		logger.debug("serializing service request, request class: {}", clazz.getName());

		String msgRequest = null;
		try {
			logger.debug("marshaling request");
			msgRequest = UniversalUnmarshaller.marshall(jaxb, clazz);
		} catch (JAXBException e) {
			e.printStackTrace();
			logger.error("error while trying marshaling message", e);
		}

		logger.debug("request serialized! message: {}", msgRequest);
		return msgRequest;
	}

	/**
	 * Десериализуем ответ 
	 * По дефолту маршалим xml, можно переопределить, например для JSON
	 * 
	 * @param msgResponse
	 * @param clazz
	 * @return
	 */
	protected <A> A deserializeAnswer(String msgResponse, Class<A> clazz) {
		A answer = null;
		if (msgResponse != null) {
			try {
				logger.debug("unmarshaling  answer");
				answer = UniversalUnmarshaller.unmarshall(msgResponse, clazz);
			} catch (JAXBException e) {
				e.printStackTrace();
				logger.error("unmarshalling error", e);
			}
		}
		return answer;
	}

	/**
	 * Создать канал из инициализированного коннекшна
	 * 
	 * @param conn
	 * @return
	 * @throws InitializeMQConnectionException 
	 */
	private Channel createChannel(Connection conn) throws InitializeMQConnectionException {
		
		if(conn == null)
			throw new InitializeMQConnectionException("MQ Connection is NULL! It is bad way, guys!");
		
		Channel channel = null;
		try {
			channel = conn.createChannel();
			logger.debug("created MQ channel");
		} catch (IOException e) {
			logger.error("error on create MQ channel", e);
		}
		return channel;
	}

	/**
	 * Закрыть канал
	 * 
	 * @param channel
	 */
	private void closeChannel(Channel channel) {

		logger.debug("closing MQ channel");

		if (channel != null && channel.isOpen()) {
			try {
				channel.close();
			} catch (IOException | TimeoutException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Получает новое соединение, если оно почему то было оборвано
	 * 
	 * @return
	 * @throws TimeoutException
	 * @throws IOException
	 */
	private Connection getConnection() throws IOException, TimeoutException {
			
		logger.info("getting new MQ connection...");
		
		Connection local = connection;
		if(local == null){
			synchronized (connection){
				local = connection;
				if(local == null){
					connection = local = factory.newConnection();
				}
			}
		}
		
		return local;
	}

	public void setFactory(ConnectionFactory factory) {
		this.factory = factory;
	}

	public void setTimeout_seconds(int timeout_seconds) {
		this.timeout_seconds = timeout_seconds;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
